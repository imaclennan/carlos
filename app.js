
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/nodemon');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(path.dirname(__dirname), 'build')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  function(username, password, done) {
    if (username == 'jimbBob') {
      return done(null, {
        username: 'jimbBob',
        token: 'abc123'
      });
    } else {
      return done(null, false);
    }
  }
));

app.use(passport.initialize());

app.get('/api', routes.index);
app.get('/users', user.list);
app.post('/users', user.create);
app.post('/api/login', function (req, res, next) {
  console.log(req.body);
  passport.authenticate('local', function (err, user, info) {
    if (!user) {
      res.send({
        status: 404,
        username: null,
        token: null
      });
    } else {
      res.send({
        status: 200,
        username: user.username,
        token: user.token
      });
    }
  })(req, res, next);
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
